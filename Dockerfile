FROM python:3-alpine 

WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN pip3 install -r requirements.txt --no-cache-dir
RUN mkdir db
COPY . /app

EXPOSE 8000

#ENTRYPOINT [ "python3" ]

#CMD ["manage.py", "runserver", "0.0.0.0:8000"]

CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000 
